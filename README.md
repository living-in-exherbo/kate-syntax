What is This
============

This is the repo w/ `kate` syntax file(s) for Paludis.

- `paludis-options.xml` syntax highlithing for [`options.conf`] file(s)


[`options.conf`]: https://paludis.exherbo.org/configuration/use.html
